import { Component, OnInit } from '@angular/core';
import { Observable }     from 'rxjs/Observable';

import { Suggestion } from '../typeahead-search/suggestion';
import { Carousel } from '../carousel/carousel';

import { DataService } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    private suggestions: Array<Suggestion> = [];
    private items: Array<Carousel> = [];
    private data: any[] = [];

    private SUGGESTIONS_API: string = 'https://api.myjson.com/bins/1bi77r';
    private CAROUSEL_ITEMS_API: string = 'https://api.myjson.com/bins/tfjlr';

    getSuggestions() {
        this.dataService.getSuggestions(this.SUGGESTIONS_API)
                        .subscribe((suggestions) => {
                            console.log('Suggestions >> ', suggestions);
                            this.suggestions = suggestions;
                        }, (err) => {
                            console.log('Error >> ', err);
                        });
    }

    getCarouselItems() {
        this.dataService.getCarouselItems(this.CAROUSEL_ITEMS_API)
                        .subscribe((items) => {
                            console.log('Carousel items >> ', items);
                            this.items = items;
                        }, (err) => {
                            console.log('Error >> ', err);
                        });
    }

    getTreeData() {
        this.data = [
		{
			label: 'a1',
			subs: [
                {
					label: 'a11',
				},
				{
					label: 'a12',
				}
			]
		},
		{
			label: 'b1',
			subs: [
				{
					label: 'b11',
				},
				{
					label: 'b12',
				}
			]
		}
	];
    }

  constructor(private dataService: DataService) { }

  ngOnInit() {
      this.getSuggestions();
      this.getCarouselItems();
      this.getTreeData();
  }

}
