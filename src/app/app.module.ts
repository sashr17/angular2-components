import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

/* COMPONENTS */
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TypeaheadSearchComponent } from './typeahead-search/typeahead-search.component';
import { CarouselComponent } from './carousel/carousel.component';

/* SERVICES */
import { DataService } from './services/data.service';

/* PIPES */
import { SuggestionFilterPipe } from './filters/suggestion-filter.pipe';
import { HighlightPipe } from './filters/highlight.pipe';
import { TreeComponent } from './tree/tree.component';
import { NodeComponent } from './tree/node/node.component';

/* ROUTES */
const appRoutes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent }
  // { path: 'search', component: TypeaheadSearchComponent },
  // { path: 'carousel', component: CarouselComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    TypeaheadSearchComponent,
    CarouselComponent,
    SuggestionFilterPipe,
    HighlightPipe,
    TreeComponent,
    NodeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
      DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
