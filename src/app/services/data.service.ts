import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Suggestion } from '../typeahead-search/suggestion';
import { Carousel } from '../carousel/carousel';

@Injectable()
export class DataService {

  constructor(private http: Http) { }

  getSuggestions(url: string): Observable<Suggestion[]> {
      return this.http.get(url)
                 // ...and calling .json() on the response to return data
                 .map((res:Response) => {
                     return res.json().suggestions || [];
                 })
                 //...errors if any
                 .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getCarouselItems(url: string): Observable<Carousel[]> {
      return this.http.get(url)
                 // ...and calling .json() on the response to return data
                 .map((res:Response) => {
                     return res.json().items || [];
                 })
                 //...errors if any
                 .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
