import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'suggestionFilter'
})
export class SuggestionFilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
      let filter = args.toLocaleLowerCase();
      return filter ? value.filter(suggestion => suggestion.name.toLocaleLowerCase().indexOf(filter) != -1) : value;
  }

}
