export class Carousel {
    private src: string;
    private thumbnailUrl: string;
    private title: string;
    private duration: string;
}
