import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit, AfterViewInit {
    @Input() private carouselItems;

    SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };

    getNextItem() {
        let item = this.carouselItems.shift();
        this.carouselItems.push(item);
    }

    getPreviousItem() {
        let item = this.carouselItems.pop();
        this.carouselItems.unshift(item);
    }

    swipe(action: string = this.SWIPE_ACTION.LEFT) {
        action === this.SWIPE_ACTION.LEFT ? this.getNextItem() : this.getPreviousItem();
    }

    next(e) {
        e.preventDefault();
        this.getNextItem();
    }

    prev(e) {
        e.preventDefault();
        this.getPreviousItem();
    }

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
      $(document).on('click', '[data-toggle="modal"]', function (event) {
          event.preventDefault();

          $("#modalVideo").attr('src', $(this).attr('href'));

          /* Get iframe src attribute value i.e. YouTube video url
          and store it in a variable */
          var url = $("#modalVideo").attr('src');

          /* Assign empty url value to the iframe src attribute when
          modal hide, which stop the video playing */
          $("#myModal").on('hide.bs.modal', function(){
              $("#modalVideo").attr('src', '');
          });

          /* Assign the initially stored url back to the iframe src
          attribute when modal is displayed again */
          $("#myModal").on('show.bs.modal', function(){
              $("#modalVideo").attr('src', url);
          });
      });
  }

}
