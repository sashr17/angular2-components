import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-typeahead-search',
  templateUrl: './typeahead-search.component.html',
  styleUrls: ['./typeahead-search.component.scss']
})
export class TypeaheadSearchComponent implements OnInit {
    @Input() private prompt;
    @Input() private suggestions;
    @Input() private title;

    private model: string = "";
    private isEscKey: boolean = false;

    handleSearch(item) {
        this.model = "";
        console.log('Item to be searched >> ', item);
    }

    handleKeys(e) {
        this.isEscKey = e.keyCode === 27 ? true : false;
    }

  constructor() { }

  ngOnInit() { }

}
